import asyncio
import aiohttp
import time
import os
import sys
import aiohttp.client_exceptions
import pandas as pd
from bs4 import BeautifulSoup


TIMEOUT = aiohttp.ClientTimeout(total=20)


async def fetch_and_parse(url):
    html = False
    status_code = False
    try:
        status_code, html = await fetch(f"http://{url}")
    except Exception as e:
        print(e)
        
    if html:
        tags, cleaned_text = process_html(html)
        write_text(tags, cleaned_text, url)

    result = {
        'domainname': url,
        'statuscode': status_code if status_code else "NULL",
        'is_downloaded': 1 if status_code == 200 else 0,
        }
    return result


async def fetch(url):
    conn = aiohttp.TCPConnector(ssl=False, limit=200)
    session = aiohttp.ClientSession(connector=conn, timeout=TIMEOUT)
    try:
        async with session.get(url) as r:
            if r.status == 200:
                status = r.status
                text = await r.text()
            else:
                status = r.status
                text = None
        await session.close()
        return status, text
    except aiohttp.client_exceptions.ClientConnectionError:
        await session.close()
        return None, None
    #best practices
    except:
        await session.close()
        return None, None


def process_html(html):
    soup = BeautifulSoup(html)
    tags = {
        "title": soup.title.string if soup.title else None,
        "keywords": extract_meta_tag(soup, 'keywords'),
        "description": extract_meta_tag(soup, 'description')
    }

    for script in soup(["script", "style"]):
        script.extract()

    cleaned_text = soup.get_text()
    return tags, cleaned_text


def extract_meta_tag(soup, tag_to_find):
    """bs4 почему-то не имеет аналога, или я плохо искал"""
    for tag in soup.find_all('meta'):
        _property = tag.get('property')

        if _property == f"og:{tag_to_find}":
            meta_tag = tag.get('content')
            return meta_tag

    return None


def write_text(tags, text, url):
    if not os.path.exists('downloads'):
        os.makedirs('downloads')

    filename = f"{url.replace('http://', '')}.txt"
    f = open(f"downloads/{filename}", "w+" , encoding="utf8")

    for k , v in tags.items():
        f.write(f"{k} : {v}\n")

    f.write(text)
    f.close()
    return True


def save_to_csv(requests_result, offset):
    fields = ['domainname', 'statuscode', 'is_downloaded']
    df = pd.DataFrame(columns=fields, data=requests_result)
    df.index += offset

    if not os.path.isfile('result_file.csv'):
        df.to_csv('result_file.csv', header=fields, encoding='utf8')
    else: 
        df.to_csv('result_file.csv', mode='a', header=False, encoding='utf8')
    return True


def load_urls(filename, chunk_size):
    csv_file = pd.read_csv(filename, chunksize=chunk_size, delimiter=',', usecols=[1], dtype={'dname':str})
    for chunk in csv_file:
        urls = chunk.values.tolist()
        yield urls


def asyncronious(chunk_size, filename):
    tasks = list()
    el = asyncio.get_event_loop()
    urls_done_counter = 0 #Нужен для правильной записи индекса в csv

    for load in load_urls(filename, chunk_size):
        tasks = asyncio.gather(*[fetch_and_parse(url[0]) for url in load])
        result = el.run_until_complete(tasks)
        save_to_csv(result, urls_done_counter)
        urls_done_counter += chunk_size
        print(f"{urls_done_counter} done")

    return True


def execute(tasks, event_loop):
    load = asyncio.gather(*tasks)
    request_results = event_loop.run_until_complete(load)
    return request_results


if __name__ == '__main__':
    domains_file = sys.argv[1]
    start = time.time()
    result = asyncronious(200, domains_file)
    print(f"elapsed time {time.time() - start}")
